#include <ProgmemParser.h>

// объявляем обработчики команд
void finishHandler(int argc, char **argv);
void testHandler(int argc, char **argv);
void helpHandler(int argc, char **argv);
void memoryPrint( const __FlashStringHelper* msg = nullptr);


/* **************************************************************
 *  данные парсера помощи
 */
namespace Help {
  const char finish_Help[] PROGMEM = "finish: Help finish. Can used with arguments";
  const char mem_Help[] PROGMEM = "Show ESP free memory/max block. No arguments requered";
  const char help_Help[] PROGMEM = "I know commands: finish, test, help\n'help finish' or 'help test' for detail information";
  
  // массив строк помощи в progmem с nullptr в конце (чтобы не считать самому)
  PGM_P const PROGMEM helpStr[] = { finish_Help, mem_Help, help_Help,
          nullptr };
  // энумератор комманд не нужен, поскольку используется индекс комманд
}


 
/* **********************************************************************
 *  парсер с обработчиками комманд
 */




// собственно парсер
namespace CommandTokens {
   // текстовые команды в progmem
  const char m1_Cmd[] PROGMEM = "finish";
  const char m2_Cmd[] PROGMEM = "memory";
  const char help_Cmd[] PROGMEM = "help";

  // массив команд в progmem
  PGM_P const PROGMEM commands[] = { m1_Cmd, m2_Cmd, help_Cmd,
      nullptr };

  // энумератор комманд   
  enum Index { finish, test, help }; 

  // массив обработчиков комманд   
  void (*funcs[])(int argc, char **argv)  = { finishHandler, testHandler, helpHandler };
};

// нужны глобальные объекты для вызова из обработчика или усложнять функции обработчика
ProgmemParser commands(CommandTokens::commands, CommandTokens::funcs );
ProgmemParser help(Help::helpStr);

// обработчик ТЕСТ
void testHandler(int argc, char **argv){
  memoryPrint(F("Esp memory:"));
};
// обработчик команды ФИНИШ
void finishHandler(int argc, char **argv){
  if ( argc > 1 ){
    switch ( commands.isToken(argv[1]) ) {
      case CommandTokens::Index::help :                             // если help
        Serial.println( help.getToken(commands.isToken(argv[0])));  // печатаем токен help по индексу команды из argv[0] 
        break;

      default:
        Serial.print(F("Handler finished with args: "));
        for ( int i = 1; i < argc; i++ ) {
            Serial.print(argv[i]);
            if ( i + 1 < argc )
              Serial.print(F(", "));
        }
        Serial.println();
      }
  } else {
    Serial.println(F("Handler finished without args"));
  }
};
// обработчик команды help для commands
void helpHandler(int argc, char **argv){
  int helpPointer = -1;
  if ( argc == 1){
    helpPointer = commands.isToken( argv[0] ); // help help
  }
    
  if ( argc > 1 ){
    //Serial.println( argv[1]);
    helpPointer = commands.isToken( argv[1] ); //
  }

//  if ( helpPointer >= 0 ){
    Serial.println( help.getToken( helpPointer, true ) );
//  }
};



//вспомогательная функция вывода памяти на экран
void memoryPrint( const __FlashStringHelper* msg )
  {
    if ( msg != nullptr )
      Serial.println(msg);
  Serial.print(F("Free heap/max size: "));
  Serial.print(ESP.getFreeHeap());
  Serial.print(F("/"));
  Serial.println(ESP.getMaxFreeBlockSize());
  
}


void prompt(){
  Serial.print(F("> "));
}
/*
 * собственно скетч
 */
void setup() {
  Serial.begin(9600);
  while( !Serial) {
    ;
  }

  Serial.println(  );
  prompt();
 }

void loop() {
  if ( Serial.available() ){
    String line = Serial.readString();

    commands.execStr(line.c_str());
    prompt();
  }
   
}
