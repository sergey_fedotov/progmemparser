#include <ProgmemParser.h>

// объявляем обработчики команд
void finishHandler(int argc, char **argv);
void testHandler(int argc, char **argv);
void helpHandler(int argc, char **argv);


/* **************************************************************
 *  данные парсера помощи
 */
namespace Help {
  const char finish_Help[] PROGMEM = "finish: Help finish. Can used with arguments";
  const char test_Help[] PROGMEM = "test: Help for test. No arguments ";
  const char help_Help[] PROGMEM = "I know commands: finish, test, help\n'help finish' or 'help test' for detail information";
  
  // массив строк помощи в progmem с nullptr в конце (чтобы не считать самому)
  PGM_P const PROGMEM helpStr[] = { finish_Help, test_Help, help_Help,
          nullptr };
  // энумератор комманд не нужен, поскольку используется индекс комманд CommandTokens
}


 
/* **********************************************************************
 *  парсер с обработчиками комманд
 */




// собственно парсер
namespace CommandTokens {
   // текстовые команды в progmem
  const char m1_Cmd[] PROGMEM = "finish";
  const char m2_Cmd[] PROGMEM = "test";
  const char help_Cmd[] PROGMEM = "help";
  const char cmdPrefix[] PROGMEM = "/";

  // массив команд в progmem
  PGM_P const PROGMEM commands[] = { m1_Cmd, m2_Cmd, help_Cmd,
      nullptr };

  // энумератор комманд   
  enum Index { finish, test, help }; 

  // массив обработчиков комманд   
  void (*funcs[])(int argc, char **argv)  = { finishHandler, testHandler, helpHandler };
};

// нужны глобальные объекты для вызова из обработчика или усложнять функции обработчика
ProgmemParser commands(CommandTokens::commands, CommandTokens::cmdPrefix, CommandTokens::funcs );
ProgmemParser help(Help::helpStr);

// обработчик ТЕСТ
void testHandler(int argc, char **argv){
  Serial.println(F("Handler test"));
};
// обработчик команды ФИНИШ
void finishHandler(int argc, char **argv){
  if ( argc > 1 ){
    Serial.print(F("Handler finished with args: "));
    for ( int i = 1; i < argc; i++ ) {
        Serial.print(argv[i]);
        if ( i + 1 < argc )
          Serial.print(F(", "));
    }
    Serial.println();
    if ( commands.isToken(argv[1]) == CommandTokens::Index::help ){
      Serial.println( help.getToken(commands.isToken(argv[0])));
    }
  } else {
    Serial.println(F("Handler finished without args"));
  }
};
// обработчик команды help для commands
void helpHandler(int argc, char **argv){
  int helpPointer = -1;
  
  switch ( argc ){
    case 0:
      break;

    case 1:
      helpPointer = commands.isToken( argv[0] ); // help help
      break;
    
    default:
    //Serial.println( argv[1]);
    helpPointer = commands.isToken( argv[1], false ); //
  }

//  if ( helpPointer >= 0 ){
    Serial.println( help.getToken( helpPointer, true ) );
//  }
};



//вспомогательная функция вывода памяти на экран
void memoryPrint( const __FlashStringHelper* msg = nullptr)
  {
    if ( msg != nullptr )
      Serial.println(msg);
  Serial.print(F("Free heap/max size: "));
  Serial.print(ESP.getFreeHeap());
  Serial.print(F("/"));
  Serial.println(ESP.getMaxFreeBlockSize());
  
}

/*
 * собственно скетч
 */
void setup() {
  Serial.begin(9600);
  while( !Serial) {
    ;
  }

  Serial.println(  );
  Serial.print("Print list of commands: ");
  Serial.println( commands);

  {
    char buf[400];
    commands.toPrint(buf, "|", "Out list commands to string: ");
    Serial.println(buf);
  }

  Serial.println( F("Exec '/help'") );
  commands.execStr("/help");

  Serial.println( F("\nExec '/help finish'") );  
  commands.execStr("/help finish");

  Serial.println( F("\nExec '/help_test'") );
  commands.execStr("/help_test");
  
  Serial.println( F("\nExec '/finish help'") );
  commands.execStr("/finish help");


  Serial.println( F("Exec 'test'") );
  commands.execStr("test");


  Serial.println( F("Exec '/test'") );
  commands.execStr("/test");

  delay(2000);
}

void loop() {
   
}
