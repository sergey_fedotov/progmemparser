#include <ProgmemParser.h>

/*  простой текстовый парсер
 *   
 */
namespace TextTokens {

  // текстовые команды в progmem
  const char start_Str[] PROGMEM = "start";
  const char help_Str[] PROGMEM = "help";
  
  const char t1[] PROGMEM = "restart";
  const char t2[] PROGMEM = "return";
  const char t3[] PROGMEM = "system";
  const char t4[] PROGMEM = "settings";
  const char t5[] PROGMEM = "reboot";
  const char t6[] PROGMEM = "memory";
  const char t7[] PROGMEM = "ls";
  const char t8[] PROGMEM = "del";

  // массив команд в progmem с nullptr в конце (чтобы не считать самому)
  PGM_P const PROGMEM tokens[] = { start_Str, help_Str, t1, t2,t3,t4,t5,t6,t7,t8,
          nullptr };
  // энумератор комманд          
  enum Index { start, help,  _t1, _t2,_t3,_t4,_t5,_t6,_t7,_t8 }; 
};
/*
 * 
 */
 

void finishHandler(int argc, char **argv);
void helpHandler(int argc, char **argv);


/* **************************************************************
 *  парсер помощи
 */
namespace Help {
  const char finish_Help[] PROGMEM = "finish: Help finish";
  const char test_Help[] PROGMEM = "test: Help for test";
  const char help_Help[] PROGMEM = "This help";
  
  // массив строк помощи в progmem с nullptr в конце (чтобы не считать самому)
  PGM_P const PROGMEM helpStr[] = { finish_Help, test_Help, help_Help,
          nullptr };
  // энумератор комманд          
  //enum Index { finish, test, help };
}


 
/* **********************************************************************
 *  парсер с обработчиками комманд
 */


// обработчик ТЕСТ
void testHandler(int argc, char **argv){
  Serial.println(F("Handler test"));
};

// собственно парсер
namespace CommandTokens {
   // текстовые команды в progmem
  const char m1_Cmd[] PROGMEM = "finish";
  const char m2_Cmd[] PROGMEM = "test";
  const char help_Cmd[] PROGMEM = "help";

  // массив команд в progmem
  PGM_P const PROGMEM commands[] = { m1_Cmd, m2_Cmd, help_Cmd,
      nullptr };

  // энумератор комманд   
  enum Index { finish, test, help }; 

  // массив обработчиков комманд   
  void (*funcs[])(int argc, char **argv)  = { finishHandler, testHandler, helpHandler };
};



//Создаем глобальный объект TEST класса парсера 
ProgmemParser test(TextTokens::tokens);


ProgmemParser commands(CommandTokens::commands, CommandTokens::funcs );
ProgmemParser help(Help::helpStr);

// обработчик команды ФИНИШ
void finishHandler(int argc, char **argv){
  if ( argc > 1 ){
    Serial.print(F("Handler finised with args: "));
    for ( int i = 1; i < argc; i++ ) {
        Serial.print(argv[i]);
        if ( i + 1 < argc )
          Serial.print(F(", "));
    }
    Serial.println();
    if ( commands.isToken(argv[1]) == CommandTokens::Index::help ){
      Serial.println( help.getToken(commands.isToken(argv[0])));
    }
  } else {
    Serial.println(F("Handler finised without args"));
  }
};
// обработчик команды help для commands
void helpHandler(int argc, char **argv){
  int helpPointer = -1;
  if ( argc == 1){
    helpPointer = commands.isToken( argv[0] ); // help help
  }
    
  if ( argc > 1 ){
    Serial.println( argv[1]);
    helpPointer = commands.isToken( argv[1] ); //
  }

//  if ( helpPointer >= 0 ){
    Serial.println( help.getToken( helpPointer, true ) );
//  }
};



//вспомогательная функция вывода памяти на экран
void memoryPrint( const __FlashStringHelper* msg = nullptr)
  {
    if ( msg != nullptr )
      Serial.println(msg);
  Serial.print(F("Free heap/max size: "));
  Serial.print(ESP.getFreeHeap());
  Serial.print(F("/"));
  Serial.println(ESP.getMaxFreeBlockSize());
  
}

/*
 * собственно скетч
 */
void setup() {
  Serial.begin(9600);
  while( !Serial) {
    ;
  }
  Serial.println( help );


  commands.execStr("help");
  commands.execStr("help finish");
  commands.execStr("help_test");
  delay(2000);


  // поиск индекса команды start
  int cmd = test.isToken("start");
  if ( cmd >= 0 ) {
    Serial.print("Found command index ");
    Serial.println( cmd );
  }

  // вывод токена команды по индексу
  Serial.println( test.getToken(cmd) );

// поиск индекса команды help и печать токена
  cmd = test.isToken("help");
  Serial.println( test.getToken(cmd) );

// поиск индекса команды Help и печать несуществующего токена
  cmd = test.isToken("Help");
  Serial.println( test.getToken(cmd, true) );

// использования switch/case для разбора команд
  cmd = test.isToken("help");
  switch ( (TextTokens::Index)cmd ) {
    case TextTokens::Index::start :
      Serial.println("This start command");
      break;
    case TextTokens::Index::help :
      Serial.println("Help string");
      break;
    default:
      Serial.println( test.getToken(-1)); 
    
  }

    memoryPrint();
    char buf[100];
    memoryPrint();
    // запрос токена в буфер с префиксом и суффиксом для печати
    test.toPrint(buf, cmd, "\t\"", "\":");
    Serial.println(buf);
    memoryPrint();
  

  }

void newParser(){
    // создание парсера test2 с обработчиками команд из namespase TestTokens1
    ProgmemParser test2(CommandTokens::commands, CommandTokens::funcs );

    // печать список всех токенов парсера
    Serial.println( test2 );
    memoryPrint(F("============ parser created ==========="));

    // запрос числа токенов в парсере ( числа команд)
    Serial.print(F("Test has entries: "));
    Serial.println( test.length() );
    Serial.println( test );

    //Serial.println( "add char []" );
    memoryPrint(F("+++++++++ add char[] before ++++++++++++"));
    // эта строка содержит команду finish с двумя аргументами this и programm
    char finishP[] = "finish_this programm";

    memoryPrint(F("====== added ===="));
    
    // запуск обработчика команды из finishP
    //test2.execStr( finishP );
    Token finishCmd( finishP, test2 );
    finishCmd.exec();
    
memoryPrint(F("====== exec finishP ===="));

    // запуск обработчика команды из test
    test2.execStr( ("test" ));


    //запрос и печать несуществующей команды myToken
    memoryPrint(F("+++++++ started handlers ++++++"));
        
    const char  myToken[]  = "myToken ";
    int cmd = test2.isToken( myToken );
    if ( cmd < 0 )
      Serial.println( test2.getToken( cmd , true) );
    else {
      test2.execStr( myToken );
    }
    memoryPrint(F("+++++++ checked unknow cmd ++++++"));
    delay( 3000 );
  

    // сохдания объекта типа Token из строки finishP для парсера test2
    Token testCmd( finishP, test2);

    // проверка парсинга и запуск команды (тоже что и test2.execStr( finishP ) )
    if ( testCmd.hasToken() )
      testCmd.exec();
      
    memoryPrint(F("====== created and exec ====\n+++++++ delay and exit ++++++"));

    delay( 3000 );


   }

void loop() {
    //Serial.println(F("================ loop ==================="));
    memoryPrint(F("================ loop ==================="));
    
    newParser();
    delay( 3000 );
    memoryPrint(F("================ newParser finished ==================="));

     String testStr = F("finish ");
    for ( int i = 0; i < 100; i++ ){
      test.execStr( (testStr+String(i)).c_str() ); /*
      Token testCmd( (testStr+String(i)).c_str(), test );
      if ( testCmd.hasToken() )
        testCmd.exec(); */
    }
    memoryPrint(F("end loop"));
  //  Serial.println(F("================ end loop ==================="));
   
}
