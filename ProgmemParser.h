#pragma once

#include <Arduino.h>

#define MAX_ARGS 5 //include self command
#define MAX_STRING_COMMAND_LENGTH 100
#define DEBUG_PARSER 0
#define DEBUG_TOKEN 0

//extern class Command;

class ProgmemParser : public Printable {
  private:
    friend class Token;
    const char * const * namePointer;         // указатель на массив указателей на токены в PROGMEM
    const char * prefix;
    void (* const * handlers)(int argc, char **argv); // указатель на массив указателей на обработчики команд
    static constexpr char notFound[] PROGMEM = "Not found ";
    static constexpr char argsSeparators[] PROGMEM = "_ \t\n\r";
    const unsigned long len;                  // число токенов в массиве
    
    long count(); // функция подсчёта числа токенов для инициализации len 

  public:
    size_t printTo(Print& p) const;

    //PGM_P const * PROGMEM as argument needed
    ProgmemParser();
    ProgmemParser(PGM_P const * pointer, PGM_P const prefix = nullptr, void (**func)(int argc, char **argv)  = nullptr );
    ~ProgmemParser();

    int isToken(const char * text, bool usePrefix = true ) const;

    unsigned long length();
    
    void execStr( const char * str);

    bool exec(int cmd, int argc, char ** argv = nullptr);
    const char * getToken(int cmd, bool showNotFound= false) const;
    unsigned long toPrint(char * buf, const int cmd, const char* before = nullptr, const char * after = nullptr);
    unsigned long toPrint(char * buf, const char* separator = nullptr, const char* before = nullptr );
  };

class Token  {
  private:
    friend class ProgmemParser;
    ProgmemParser *parser;
    char * command; //[MAX_STRING_COMMAND_LENGTH];
    int argc;
    char * argv[MAX_ARGS];

    char* init(const char * cmd, ProgmemParser* p);
    char* init(const char * cmd);
  public:
    //Command();
    Token(const char * cmd = nullptr, ProgmemParser* p = nullptr);
    Token(const char * cmd, ProgmemParser& p);
    ~Token();
    bool hasToken();
    bool hasArgs();
    void exec();
};