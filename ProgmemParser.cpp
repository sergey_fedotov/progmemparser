#include <ProgmemParser.h>

//class ProgmemParser : public Printable {
    
    long ProgmemParser::count(){
      unsigned long c = 0;
      while ( this->namePointer[c] != nullptr )
        c++;
      return c;
    };

    size_t ProgmemParser::printTo (Print& p) const {
      size_t res =0;
      uint i = 0;
      while ( true ){
        res += p.print( this->namePointer[i] );
        i++;
        if ( this->namePointer[i] == nullptr ) {
          break;
        }
        res += p.print( F(", "));
      }
      return res;
    };

    //PGM_P const * PROGMEM as argument needed
    ProgmemParser::ProgmemParser(PGM_P const * pointer, PGM_P const prefix, void (**func)(int argc, char **argv)  ) :
      namePointer ( pointer ), len ( ProgmemParser::count() ), handlers ( func ), prefix( prefix )
    {
      };
    ProgmemParser::~ProgmemParser(){
      };

    int ProgmemParser::isToken(const char * text,  bool usePrefix ) const {
      int index = -1;
      if ( text != nullptr && text[0] != '\0' ){

        int prefixLength = usePrefix && this->prefix != nullptr ? strlen(this->prefix) : 0;
        if ( prefixLength != 0 ) {
          if ( strncmp( text, this->prefix, prefixLength )){
            // начало не совпало с префиксом
            return index;
          }
        }

        uint i = 0;
        while ( i < len ){
            if ( !strcmp(&text[prefixLength], this->namePointer[i] )){
              index = i;
              break;
            } else 
              i++;
        }
      }
      return index;
    };

    unsigned long ProgmemParser::length(){
      return this->len;
    };
    
    void ProgmemParser::execStr( const char * str){
/*
      Command * cmd = new Command();
      cmd->init(str, this);
      cmd->exec();
      delete cmd; */
      Token cmd(str, this);
      cmd.exec(); 
    };

    bool ProgmemParser::exec(int cmd, int argc, char ** argv ){
      if (  cmd < 0 || 
            cmd >= (long)len || 
            handlers == nullptr || 
            handlers[cmd] == nullptr )
        return false;
      this->handlers[cmd](argc, argv );
      return true;
    };
    const char * ProgmemParser::getToken(int cmd, bool showNotFound) const {
      if ( cmd < 0 || cmd >= (long)len )
        if ( showNotFound )
          return notFound;
        else 
          return nullptr;
      else
        return this->namePointer[cmd];
    };

    //заполняет буфер buf именем cmd с префиксом before и постфиксом after для вывода куда-либо
    unsigned long ProgmemParser::toPrint(char * buf, const int cmd, const char* before, const char * after ){
      unsigned long count = 0;
      
      if ( before != nullptr ){
         strcpy(buf + count, before);
         count += strlen(before);
      }
      
      if ( cmd < 0 ){
         strcpy(buf + count, notFound);
         count += strlen(notFound);
      } else {
         strcpy(buf + count, this->namePointer[cmd]);
        count += strlen(this->namePointer[cmd]);
      }
      if ( after != nullptr ){
         strcpy(buf + count, after);
        count += strlen(after);
      }
      return count;
    };

    //заполняет буфер buf списком именем cmd с префиксом before и разделителем separator для вывода куда-либо
    unsigned long ProgmemParser::toPrint(char * buf, const char* separator, const char* before ){
      unsigned long count = 0;
      
      if ( before != nullptr ){
         strcpy(buf , before);
         count += strlen(before);
      }
      int i =0;
      while ( true ){
        strcpy(buf + count, this->namePointer[i] );
        count += strlen( this->namePointer[i] );
        i++;
        if ( this->namePointer[i] == nullptr ) {
          break;
        }
        if ( separator != nullptr ){
          strcpy(buf + count, separator );
          count += strlen( separator );
        }
      }
      return count;
    };

// class Token  {

    char* Token::init(const char * cmd, ProgmemParser* p){
      parser = p;
      return init(cmd);
    }
    char* Token::init(const char * cmd){
      int len = strlen(cmd);
#if DEBUG_TOKEN
      Serial.print(F("Input str len="));
      Serial.println(len);
#endif
      this->command =  (char*)malloc(sizeof(char)*len+1);
      strncpy(this->command, cmd, len);
      this->command[len] = '\0';
#if DEBUG_TOKEN
      Serial.print(F("Input copyed="));
      Serial.println(this->command);
#endif

      this->argc = 0;      
      argv[this->argc] =  strtok( this->command, ProgmemParser::argsSeparators);

      do
      { 
#if DEBUG_TOKEN
      Serial.print(F("separete arg["));
      Serial.print(this->argc);
      Serial.print(F("]="));
      Serial.println(argv[this->argc]);
#endif
        if ( argv[this->argc] == NULL || strlen(argv[this->argc]) >= 1 ) {
          this->argc += 1;
        }
        argv[this->argc] = strtok(NULL, ProgmemParser::argsSeparators);   
         
      } while ((this->argc < MAX_ARGS) && (argv[this->argc] != NULL));

#if DEBUG_TOKEN
      Serial.print(F("argc="));
      Serial.println(argc);
#endif
      return this->command;
    };
  
    //Command::Command(){};
    Token::Token(const char * cmd, ProgmemParser& p) :
      command( init( cmd )), parser( &p )
    {};
    Token::Token(const char * cmd, ProgmemParser* p) :
      command( init( cmd )), parser(p) 
    {};
    Token::~Token(){
      delete this->command;
    };
    bool Token::hasToken(){
      return this->parser->isToken(argv[0]) >= 0;
    };
    bool Token::hasArgs(){
      return argc > 1;
    };
    void Token::exec(){
      this->parser->exec( this->parser->isToken(argv[0]), argc, argv);
    };
